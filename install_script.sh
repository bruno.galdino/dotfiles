#Install Script

#Variables
RUBY_VERSION="2.5.0"
NODE_VERSION="9.5.0"
GOLANG_VERSION="1.9.2"
DIR_PATH="~/Downloads/dotfiles"

# Prepare pacman
sudo mkdir -p /root/.gnupg
sudo pacman-key --init && sudo pacman-key --populate archlinux manjaro && sudo pacman-key --refresh-keys

#Upgrading all packages
sudo pacman -Syyu --noconfirm

#Custom optimizations
echo "tmpfs /tmp tmpfs rw,nosuid,nodev" | sudo tee -a /etc/fstab

sudo tee -a /etc/sysctl.d/99-sysctl.conf <<-EOF
vm.swappiness=1
vm.vfs_cache_pressure=50
EOF

sudo tee -a /etc/sysctl.d/99-sysctl.conf <<-EOF
vm.dirty_background_bytes=16777216
vm.dirty_bytes=50331648
EOF

#Installing common applications
sudo pacman -S --noconfirm urxvt-unicode yaourt zsh arc-gtk-theme mopidy
yaourt -S --noconfirm google-chrome ncmpcpp nerd-fonts-complete spotify neofetch visual-studio-code-bin polybar postman-bin fasd-git rofi-git nerd-fonts-source-code-pro

#Copying polybar
yes | cp -f $DIR_PATH/.config/polybar/config $HOME/.config/polybar
yes | cp -f $DIR_PATH/.config/polybar/launch.sh $HOME/.config/polybar
sudo chmod +x $HOME/.config/polybar/launch.sh
sh $HOME/.config/polybar/launch.sh

#Copying i3 config file
yes | cp -f $DIR_PATH/.config/i3/config $HOME/.i3/
i3-msg reload
i3-msg restart

#Copying dunst
yes | cp -f $DIR_PATH/.config/dunst/dunstrc $HOME/.config/dunst

#VIM
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim:q
yes | cp -f $DIR_PATH/.vimrc $HOME
vim +PluginInstall +qall

#XResource
yes | cp -f $DIR_PATH/.Xresource ~/.Xresources

#Configure ZSH
git clone --recursive git@gitlab.com:bruno.galdino/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezt""o/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done

ZSH_PATH="$(which zsh)"
sudo chsh -s $ZSH_PATH

yes | cp -f $DIR_PATH/.zshrc $HOME/.zshrc

#Install Docker
yaourt -S --noconfirm docker
sudo groupadd docker
sudo usermod -aG docker $USER
systemctl enable docker.service

#Config Ruby and Rails
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable

# Load RVM into a shell session *as a function*
if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then

  \# First try to load from a user install
  source "$HOME/.rvm/scripts/rvm"

elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then

  \# Then try to load from a root install
  source "/usr/local/rvm/scripts/rvm"

else

  printf "ERROR: An RVM installation was not found.\n"

fi

rvm install "ruby-$RUBY_VERSION"
rvm use "ruby-$RUBY_VERSION"
gem install nokogiri rails rspec

#Setup ASDF
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.4.2
source $HOME/.zshrc

#Install NODE
asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring
asdf install nodejs $NODE_VERSION
asdf global nodejs $NODE_VERSIONa
npm i -g yarn
asdf reshim nodejs
yarn global add --dev eslint 
yarn global add --dev babel-eslint 
yarn global add --dev eslint-plugin-react
yarn global add --dev prettier 
yarn global add --dev eslint-config-prettier 
yarn global add --dev eslint-plugin-prettier

#Install GOLANG
asdf plugin-add golang https://github.com/kennyp/asdf-golang.git
asdf install golang $GOLANG_VERSION
asdf global golang $GOLANG_VERSION

#Set Chrome as default browser
echo "Script finished, you need to reboot the computer for changes to take effect"

#rm -rf $DIR_PATH
