#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

export LANG=en

# Reference: http://superuser.com/questions/417627/oh-my-zsh-history-completion
bindkey '^[OA' history-beginning-search-backward
bindkey '^[OB' history-beginning-search-forward


# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Custom Exports
export LANG="en_US.utf8"
export LC_MESSAGES="C"
export LC_ALL="en_US.utf8"
export EDITOR="vim"

eval "$(fasd --init auto)"
alias v='f -e vim'

. $HOME/.asdf/asdf.sh

. $HOME/.asdf/completions/asdf.bash
